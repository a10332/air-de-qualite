"""
Ce programme utilise un Raspberry Pi modèle B rev2 avec un capteur MH-Z14A pour lire la concentration de CO2
d'une pièce. La communication fonctionne en UART. Il faut activer le port sériel en utilisant raspi-config et
redémarrer le RPI avant d'utiliser ce script. Il faut aussi sudo apt-get install pip3 et pip install pyserial
pour que le import serial fonctionne.

Copyright (C) 2023 Alpaca Balena.

Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
licence, soit (à vous de voir...) toute autre version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
connaissance de la Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
cas, voir <https://www.gnu.org/licenses/>.
"""
from dataclasses import dataclass
import logging
from prometheus_client import start_http_server, Gauge
import time

from capteurs.mhz14a import Mhz14a
from capteurs.sgp30 import Sgp30


@dataclass(frozen=True)
class Concentrations:
    """Concentrations mesurées."""
    co2_mhz14a: int
    valeur_inconnue_mhz14a: int
    co2_sgp30: int
    cov_sgp30: int


class Publier:
    def __init__(self, journal: logging.Logger):
        """Configurer la publication."""
        self._journal = journal
        self._journal.info("Configuration de Publier pour prometheus...")

        start_http_server(43567)

        self.jauge_co2_mhz14a = Gauge(
            "concentration_co2_mhz14a",
            "Concentration de CO2 mesurée par le MHZ14A (ppm)",
        )
        self.jauge_valeur_inconnue_mhz14a = Gauge(
            "valeur_inconnue_mhz14a",
            "Valeur inconnue produite par le MHZ14A",
        )
        self.jauge_co2_sgp30 = Gauge(
            "concentration_co2_sgp30",
            "Concentration de CO2 estimée par le SGP30 (ppm)",
        )
        self.jauge_cov_sgp30 = Gauge(
            "concentration_composes_organiques_volatiles",
            "Concentration de composés organiques volatiles (ppm)",
        )
        self._journal.info("Fin de la configuration de Publier pour prometheus...")

    def envoyer(self, concentrations: Concentrations):
        """Envoyer les valeurs vers Prometheus."""
        self.jauge_co2_mhz14a.set(concentrations.co2_mhz14a)
        self.jauge_valeur_inconnue_mhz14a.set(concentrations.valeur_inconnue_mhz14a)
        self.jauge_co2_sgp30.set(concentrations.co2_sgp30)
        self.jauge_cov_sgp30.set(concentrations.cov_sgp30)


def obtenir_journal() -> logging.Logger:
    """Obtenir un journal dans lequel écrire ce qui ce passe dans le code."""
    journal = logging.Logger("mesure_qualite_air")
    journal.setLevel(logging.INFO)
    manipulateur_de_journal = logging.StreamHandler()
    manipulateur_de_journal.setLevel(logging.INFO)

    format_journal = "%(asctime)s - %(levelname)s - %(message)s"
    format_date = "%Y-%m-%d %H:%M:%S"
    syntaxe = logging.Formatter(format_journal, format_date)

    manipulateur_de_journal.setFormatter(syntaxe)

    journal.addHandler(manipulateur_de_journal)
    return journal


def mesurer():
    """Mesurer la qualité de l'air et la publier."""
    journal = obtenir_journal()
    sgp30 = Sgp30(journal)
    mhz14a = Mhz14a(journal)
    publier = Publier(journal)
    while True:
        mhz14a.mesurer()
        sgp30.mesurer()
        concentrations = Concentrations(
            co2_mhz14a=mhz14a.co2_ppm,
            valeur_inconnue_mhz14a=mhz14a.valeur_inconnue,
            co2_sgp30=sgp30.co2_ppm,
            cov_sgp30=sgp30.cov_ppm,
        )
        journal.info(f"Concentrations mesurées: {concentrations}.")
        publier.envoyer(concentrations)
        time.sleep(3)


if __name__ == "__main__":
    mesurer()
