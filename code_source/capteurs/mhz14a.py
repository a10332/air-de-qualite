"""
Ce programme utilise un Raspberry Pi modèle B rev2 avec un capteur MH-Z14A pour lire la concentration de CO2
d'une pièce. La communication fonctionne en UART. Il faut activer le port sériel en utilisant raspi-config et
redémarrer le RPI avant d'utiliser ce script. Il faut aussi sudo apt-get install pip3 et pip install pyserial
pour que le import serial fonctionne.

Copyright (C) 2023 Alpaca Balena.

Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
licence, soit (à vous de voir...) toute autre version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
connaissance de la Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
cas, voir <https://www.gnu.org/licenses/>.
"""
import logging
import serial


class Mhz14a:
    """Encapsule la logique pour lire le CO2."""

    _DEMANDE_NIVEAU_CO2 = bytearray([0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79])

    def __init__(self, journal: logging.Logger):
        """Initialise la communication avec le MHZ14A."""
        self._journal = journal
        self._journal.info("Début de la configuration du MHZ14A...")
        port_seriel_uart = "/dev/ttyAMA0"
        vitesse_de_communication_en_baud = 9600
        self._canal_uart = serial.Serial(
            port=port_seriel_uart,
            baudrate=vitesse_de_communication_en_baud,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1,
        )
        self.co2_ppm = 0
        self.valeur_inconnue = 0
        self._journal.info("Fin de la configuration du MHZ14A!")

    def mesurer(self):
        """Demande au MHZ14A la valeur actuelle de ses variables."""
        self._canal_uart.write(Mhz14a._DEMANDE_NIVEAU_CO2)
        reponse = self._canal_uart.read(9)
        if len(reponse) == 9:
            self.co2_ppm = (reponse[2] << 8) | reponse[3]
            self.valeur_inconnue = reponse[4]
        else:
            raise ValueError(
                f"Réception d'une réponse de longuer {len(reponse)} alors que nous attendions une réponse "
                f"de longueur 9. Réponse complète : {reponse}."
            )
