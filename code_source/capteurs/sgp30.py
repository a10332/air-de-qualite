"""
Ce programme utilise un Raspberry Pi modèle B rev2 avec un capteur Adafruit SGP30. Ce capteur a une précision
de 15% pour mesurer le taux total de composés organiques volatiles en parties par milliard. Il retourne aussi
une valeur équivalente de CO2 dans l'air en partie par million. Ce capteur n'est pas un «vrai» capteur de CO2
comme le MH-Z14A, mais il semble suivre relativement bien l'augmentation ou la diminution du CO2 d'une pièce.

AU PREMIER DÉMARRAGE, IL EST POSSIBLE QUE LES VALEURS LUES SOIENT DE 400 ET 0 POUR UN LONG MOMENT. APRÈS
QUELQUES MINUTES OU QUELQUES HEURES, LES MESURES SEMBLENT MEILLEURES.

Copyright (C) 2023 Alpaca Balena.

Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
licence, soit (à vous de voir...) toute autre version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
connaissance de la Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
cas, voir <https://www.gnu.org/licenses/>.
"""
import logging
from sgp30 import SGP30


class Sgp30:
    """Encapsule la logique de communication avec le SGP30."""

    def __init__(self, journal: logging.Logger):
        """Initialise la communication avec le SGP30."""
        self._journal = journal
        self._journal.info("Début de la configuration du SGP30...")
        self._sgp30 = SGP30()
        self._sgp30.start_measurement(self._progression)
        self.co2_ppm = 0
        self.cov_ppm = 0
        self._journal.info("Fin de la configuration du SGP30!")

    def _progression(self):
        """Imprime une barre de progression."""
        self._journal.info("▮")

    def mesurer(self):
        mesure = self._sgp30.get_air_quality()
        self.co2_ppm = mesure.equivalent_co2
        self.cov_ppm = mesure.total_voc
